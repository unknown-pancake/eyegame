const std = @import("std");


pub fn build(b: *std.Build) void {
  const target = b.standardTargetOptions(.{});
  const optimize = b.standardOptimizeOption(.{});

  const raylib_lib = buildRaylib(b, target, optimize);
  const raylib_mod = createRaylibModule(b);
  const eyegine_mod = createEyegineModule(b, raylib_mod);
 
  buildGame(b, target, optimize, raylib_mod, raylib_lib, eyegine_mod);
  buildEyeditor(b, target, optimize, raylib_mod, raylib_lib, eyegine_mod);

  const eyegine_tests = buildEyegineTests(b, target, optimize, raylib_mod, raylib_lib);
  const eyeditor_tests = buildEyeditorTests(b, target, optimize, raylib_mod, raylib_lib, eyegine_mod);
  const game_tests = buildGameTests(b, target, optimize, raylib_mod, raylib_lib, eyegine_mod);

  const run = b.step("test", "Run EYEgine's, EYEditor's, and the game's tests");
  run.dependOn(&eyegine_tests.step);
  run.dependOn(&eyeditor_tests.step);
  run.dependOn(&game_tests.step);
}




fn buildEyeditor(
  b: *std.Build,
  target: std.zig.CrossTarget, 
  optimize: std.builtin.Mode,
  raylib_mod: *std.Build.Module,
  raylib_lib: *std.Build.CompileStep,
  eyegine_mod: *std.Build.Module
) void {
  const exe = b.addExecutable(.{ 
    .name = "eyeditor",
    .root_source_file = .{ .path = "src/eyeditor/main.zig" },
    .target = target,
    .optimize = optimize,
  });

  exe.addModule("raylib", raylib_mod);
  exe.addModule("eyegine", eyegine_mod);
  exe.linkLibrary(raylib_lib);
  exe.install();

  const run = b.addRunArtifact(exe);
  run.step.dependOn(&exe.install_step.?.step);

  if(b.args) |args|
    run.addArgs(args);

  const run_step = b.step("e.run", "Run the editor");
  run_step.dependOn(&run.step);
}

fn buildGame(
  b: *std.Build,
  target: std.zig.CrossTarget, 
  optimize: std.builtin.Mode,
  raylib_mod: *std.Build.Module,
  raylib_lib: *std.Build.CompileStep,
  eyegine_mod: *std.Build.Module
) void {
  const exe = b.addExecutable(.{ 
    .name = "game",
    .root_source_file = .{ .path = "src/game/main.zig" },
    .target = target,
    .optimize = optimize,
  });

  exe.addModule("raylib", raylib_mod);
  exe.addModule("eyegine", eyegine_mod);
  exe.linkLibrary(raylib_lib);
  exe.install();

  const run = exe.run();
  run.step.dependOn(&exe.install_step.?.step);

  if(b.args) |args|
    run.addArgs(args);

  const run_step = b.step("g.run", "Run the game");
  run_step.dependOn(&run.step);
}

fn buildGameTests(
  b: *std.Build,
  target: std.zig.CrossTarget, 
  optimize: std.builtin.Mode,
  raylib_mod: *std.Build.Module,
  raylib_lib: *std.Build.CompileStep,
  eyegine_mod: *std.Build.Module
) *std.Build.RunStep {
  const exe = b.addTest(.{ 
    .name = "game_tests",
    .kind = .test_exe,
    .root_source_file = .{ .path = "src/game/main.zig" },
    .target = target,
    .optimize = optimize,
  });

  exe.addModule("raylib", raylib_mod);
  exe.addModule("eyegine", eyegine_mod);
  exe.linkLibrary(raylib_lib);
  exe.install();

  const run = exe.run();
  run.step.dependOn(&exe.install_step.?.step);

  const run_step = b.step("g.test", "Run the game's tests");
  run_step.dependOn(&run.step);

  return run;
}

fn buildEyeditorTests(
  b: *std.Build,
  target: std.zig.CrossTarget, 
  optimize: std.builtin.Mode,
  raylib_mod: *std.Build.Module,
  raylib_lib: *std.Build.CompileStep,
  eyegine_mod: *std.Build.Module
) *std.build.RunStep {
  const exe = b.addTest(.{ 
    .name = "eyeditor_tests",
    .kind = .test_exe,
    .root_source_file = .{ .path = "src/eyeditor/main.zig" },
    .target = target,
    .optimize = optimize,
  });

  exe.addModule("raylib", raylib_mod);
  exe.addModule("eyegine", eyegine_mod);
  exe.linkLibrary(raylib_lib);
  exe.install();

  const run = b.addRunArtifact(exe);
  run.step.dependOn(&exe.install_step.?.step);

  const run_step = b.step("e.test", "Run the editor's tests");
  run_step.dependOn(&run.step);

  return run;
}

fn buildEyegineTests(
  b: *std.Build,
  target: std.zig.CrossTarget, 
  optimize: std.builtin.Mode,
  raylib_mod: *std.Build.Module,
  raylib_lib: *std.Build.CompileStep
) *std.Build.RunStep {
  const tests = b.addTest(.{
    .name = "eyegine_tests",
    .kind = .test_exe,
    .root_source_file = .{ .path = "eyegine/tests.zig" },
    .target = target,
    .optimize = optimize,
  });

  tests.addModule("raylib", raylib_mod);
  tests.linkLibrary(raylib_lib);
  tests.install();

  const run = b.addRunArtifact(tests);
  run.step.dependOn(&tests.install_step.?.step);

  if(b.args) |args|
    run.addArgs(args);

  const run_step = b.step("eg.test", "Run EYEgine's tests");
  run_step.dependOn(&run.step);

  return run;
}



fn createEyegineModule(
  b: *std.Build,
  raylib: *std.Build.Module
) *std.build.Module {
  return b.createModule(.{
    .source_file = .{ .path = "eyegine/eyegine.zig" },
    .dependencies = &[_]std.Build.ModuleDependency { 
      .{ .name = "raylib", .module = raylib },
    }
  });
}

fn createRaylibModule(
  b: *std.Build
) *std.build.Module {
  return b.createModule(.{
    .source_file = .{ .path = "raylib/raylib.zig" },
  });
}



fn buildRaylib(
  b: *std.Build, 
  target: std.zig.CrossTarget, 
  optimize: std.builtin.Mode
) *std.build.CompileStep {
  const srcdir = "ext/raylib/src";
  const raylib_flags = &[_][]const u8{
    "-std=gnu99",
    "-DPLATFORM_DESKTOP",
    "-DGL_SILENCE_DEPRECATION=199309L",
    "-fno-sanitize=undefined", // https://github.com/raysan5/raylib/issues/1891
  };

  const lib = b.addStaticLibrary(.{
    .name = "raylib",
    .target = target,
    .optimize = optimize,
  });
  lib.linkLibC();

  lib.addIncludePath(srcdir ++ "/external/glfw/include");

  lib.addCSourceFiles(&.{
    srcdir ++ "/raudio.c",
    srcdir ++ "/rcore.c",
    srcdir ++ "/rmodels.c",
    srcdir ++ "/rshapes.c",
    srcdir ++ "/rtext.c",
    srcdir ++ "/rtextures.c",
    srcdir ++ "/utils.c",
  }, raylib_flags);
  
  switch (lib.target.toTarget().os.tag) {
    .windows => {
      lib.addCSourceFiles(&.{srcdir ++ "/rglfw.c"}, raylib_flags);
      lib.linkSystemLibrary("winmm");
      lib.linkSystemLibrary("gdi32");
      lib.linkSystemLibrary("opengl32");
      lib.addIncludePath(srcdir ++ "/external/glfw/deps/mingw");
    },
    .linux => {
      lib.addCSourceFiles(&.{srcdir ++ "/rglfw.c"}, raylib_flags);
      lib.linkSystemLibrary("GL");
      lib.linkSystemLibrary("rt");
      lib.linkSystemLibrary("dl");
      lib.linkSystemLibrary("m");
      lib.linkSystemLibrary("X11");
    },
    .freebsd, .openbsd, .netbsd, .dragonfly => {
      lib.addCSourceFiles(&.{srcdir ++ "/rglfw.c"}, raylib_flags);
      lib.linkSystemLibrary("GL");
      lib.linkSystemLibrary("rt");
      lib.linkSystemLibrary("dl");
      lib.linkSystemLibrary("m");
      lib.linkSystemLibrary("X11");
      lib.linkSystemLibrary("Xrandr");
      lib.linkSystemLibrary("Xinerama");
      lib.linkSystemLibrary("Xi");
      lib.linkSystemLibrary("Xxf86vm");
      lib.linkSystemLibrary("Xcursor");
    },
    .macos => {
      // On macos rglfw.c include Objective-C files.
      const raylib_flags_extra_macos = &[_][]const u8{
        "-ObjC",
      };
      lib.addCSourceFiles(
        &.{srcdir ++ "/rglfw.c"},
        raylib_flags ++ raylib_flags_extra_macos,
      );
      lib.linkFramework("Foundation");
    },
    else => {
      @panic("Unsupported OS");
    },
  }

  return lib;
}