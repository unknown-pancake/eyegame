
const std = @import("std");
const rl = @import("raylib");



pub fn main() void {
  std.log.info("EYEditor hehehe v0.1", .{});

  rl.InitWindow(1920, 1080, "Game");
  defer rl.CloseWindow();

  while(!rl.WindowShouldClose()) {
    rl.BeginDrawing();
      rl.ClearBackground(rl.BLACK);
      rl.DrawText("Hello from eyeditor/main.zig", 8, 8, 24, rl.RAYWHITE);
    rl.EndDrawing();
  }
}
