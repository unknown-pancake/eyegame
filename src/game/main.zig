// pub fn main() void {
//   _ = Test.foo.bar();
// }

// const Test = struct {
//     pub const foo = Test { .a = 1 };

//     a: f32 = 0,

//     pub inline fn bar(
//         self: Test
//     ) Ret {
//         const a = @floatToInt(i32, self.a * 256.0);

//         return .{
//             .a = @truncate(u8, @bitCast(u32, a)),
//         };
//     }

//     pub const Ret = struct {
//         a: u8, 
//     };
// };


const std = @import("std");
const eg = @import("eyegine");
const rl = @import("raylib");



pub fn main() !void {
  var gpa = std.heap.GeneralPurposeAllocator(.{}) {};
  defer _ = gpa.deinit();

  const alloc = gpa.allocator();


  rl.InitWindow(1920, 1080, "Game");
  defer rl.CloseWindow();

  var retina = eg.sight.Retina.evoke(320, 180);
  defer retina.banish();

  var matter_compendium = eg.memories.MatterCompendium.evoke(
    16, 16, 
    &[_]eg.memories.MatterCompendium.Fragment {
      .{ .texture = 0, .x = 4, .y = 0 },
      .{ .texture = 0, .x = 1, .y = 0 },
    },
    &[_]rl.Texture2D { 
      rl.LoadTexture("tileset.png")
    }
  );

  var matter_assembly = try eg.sight.MatterAssembly.evoke(
    alloc, &matter_compendium,
    4, 4
  );
  defer matter_assembly.banish(alloc);

  matter_assembly.growPlaceAt(0, 0, 1, .{});
  matter_assembly.growPlaceAt(1, 0, 1, .{});
  matter_assembly.growPlaceAt(2, 0, 1, .{});
  matter_assembly.growPlaceAt(3, 0, 1, .{});
  matter_assembly.growPlaceAt(0, 1, 0, .{});
  matter_assembly.growPlaceAt(1, 1, 0, .{});
  matter_assembly.growPlaceAt(2, 1, 0, .{});
  matter_assembly.growPlaceAt(3, 1, 0, .{});
  matter_assembly.growPlaceAt(0, 2, 0, .{});
  matter_assembly.growPlaceAt(1, 2, 0, .{});
  matter_assembly.growPlaceAt(2, 2, 0, .{});
  matter_assembly.growPlaceAt(3, 2, 0, .{});
  matter_assembly.growPlaceAt(0, 3, 0, .{});
  matter_assembly.growPlaceAt(1, 3, 0, .{});
  matter_assembly.growPlaceAt(2, 3, 0, .{});
  matter_assembly.growPlaceAt(3, 3, 0, .{});

  while(!rl.WindowShouldClose()) {
    retina.invoke();
      retina.birth();

      matter_assembly.imprint(
        eg.brain.Location2f.here,
        eg.brain.Location2f.evoke(320, 180)
      );

    retina.release();

    rl.BeginDrawing();
      retina.imprint();
    rl.EndDrawing();
  }
}
