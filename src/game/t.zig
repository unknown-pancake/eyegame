const Test = @This();

pub const foo = Test { .a = 1 };

a: f32 = 0,

pub inline fn bar(
  self: Test
) Ret {
  const a = @floatToInt(i32, self.a * 256.0);

  return .{
    .a = @truncate(u8, @bitCast(u32, a)),
  };
}

pub const Ret = extern struct {
  a: u8, 
};