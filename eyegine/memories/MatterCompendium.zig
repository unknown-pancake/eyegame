
const std = @import("std");
const rl = @import("raylib");
const eg = @import("../eyegine.zig");

const Allocator = std.mem.Allocator;
const MatterCompendium = @This();



fragment_width: u32,
fragment_height: u32,

fragments: []const Fragment,
textures: []const rl.Texture2D,

own_fragments: bool = false,
own_textures: bool = false,



pub fn evoke(
  fragment_width: u32,
  fragment_height: u32,
  fragments: []const Fragment,
  textures: []const rl.Texture2D
) MatterCompendium {
  return .{
    .fragment_width = fragment_width,
    .fragment_height = fragment_height,
    .fragments = fragments,
    .textures = textures,
  };
}

pub fn banish(
  self: *MatterCompendium,
  alloc: Allocator
) void {
  if( self.own_fragments )
    alloc.free(self.fragments);
  
  if( self.own_textures ) {
    for( self.textures ) |tex|
      rl.UnloadTexture(tex);
    
    alloc.free(self.textures);
  }
}



pub inline fn recallFragmentTexture(
  self: MatterCompendium,
  fragment: usize
) rl.Texture2D {
  return self.textures[self.fragments[fragment].texture];
}

pub inline fn recallFragmentX(
  self: MatterCompendium,
  fragment: usize
) usize {
  return self.fragments[fragment].x;
}

pub inline fn recallFragmentY(
  self: MatterCompendium,
  fragment: usize
) usize {
  return self.fragments[fragment].y;
}

pub fn recallFragmentFigure(
  self: MatterCompendium,
  fragment: usize
) rl.Rectangle {
  const x = self.recallFragmentX(fragment);
  const y = self.recallFragmentY(fragment);

  return .{
    .x = @intToFloat(f32, x * self.fragment_width),
    .y = @intToFloat(f32, y * self.fragment_height),
    .width = @intToFloat(f32, self.fragment_width),
    .height = @intToFloat(f32, self.fragment_height)
  };
}



pub const Fragment = packed struct(u32) {
  texture: u8,
  x: u12,
  y: u12,
};
