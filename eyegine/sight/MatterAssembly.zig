const std = @import("std");
const rl = @import("raylib");
const eg = @import("../eyegine.zig");

const Allocator = std.mem.Allocator;
const Location2f = eg.brain.Location2f;
const Location2i = eg.brain.Location2i;
const MatterCompendium = eg.memories.MatterCompendium;
const MatterAssembly = @This();



compendium: *const MatterCompendium,
places: []Place,

width: u32,
height: u32,



pub fn evoke(
  alloc: Allocator,
  compendium: *const MatterCompendium,
  width: u32,
  height: u32,
) Allocator.Error!MatterAssembly {
  var places = try alloc.alloc(Place, width * height);
  std.mem.set(Place, places, .{});

  return MatterAssembly {
    .compendium = compendium,
    .places = places,
    .width = width,
    .height = height,
  };
}

pub fn banish(
  self: *MatterAssembly,
  alloc: Allocator
) void {
  alloc.free(self.places);
}



pub inline fn transmuteLocationToSymbol(
  self: MatterAssembly,
  h: u32,
  s: u32
) usize {
  return @intCast(usize, h + s * self.width);
}

pub inline fn transmuteSymbolToLocation(
  self: MatterAssembly,
  symbol: usize
) eg.brain.Location2u {
  const h = @intCast(u32, symbol % self.width);
  const s = @intCast(u32, symbol / self.width);
  return .{ .horizon = h, .sky = s };
}



pub inline fn recallPlaceExistance(
  self: MatterAssembly,
  symbol: usize
) bool {
  return self.places[symbol].exists;
}

pub inline fn recallPlaceFragment(
  self: MatterAssembly,
  symbol: usize
) usize {
  return @intCast(usize, self.places[symbol].fragment);
}

pub inline fn recallPlaceWidthInvertion(
  self: MatterAssembly,
  symbol: usize
) bool {
  return self.places[symbol].invert_width;
}

pub inline fn recallPlaceHeightInvertion(
  self: MatterAssembly,
  symbol: usize
) bool {
  return self.places[symbol].invert_height;
}

pub inline fn recallPlaceRevolution(
  self: MatterAssembly,
  symbol: usize
) bool {
  return self.places[symbol].revolve;
}



pub inline fn birthPlace(
  self: *MatterAssembly,
  symbol: usize
) void {
  self.places[symbol].exists = false;
}

pub inline fn birthPlaceAt(
  self: *MatterAssembly,
  hor: u32,
  sky: u32
) void {
  self.birthPlace(self.transmuteLocationToSymbol(hor, sky));
}

pub fn growPlace(
  self: *MatterAssembly,
  symbol: usize,
  fragment: usize,
  options: GrowOptions
) void {
  self.places[symbol] = .{
    .fragment = @intCast(u28, fragment),
    .exists = true,
    .invert_width = options.invert_width,
    .invert_height = options.invert_height,
    .revolve = options.revolve
  };
}

pub inline fn growPlaceAt(
  self: *MatterAssembly,
  hor: u32,
  sky: u32,
  fragment: usize,
  options: GrowOptions
) void {
  self.growPlace(
    self.transmuteLocationToSymbol(hor, sky),
    fragment, options
  );
}



pub fn imprint(
  self: MatterAssembly,
  origin: Location2f,
  end: Location2f,
) void {
  const frag_width = self.compendium.fragment_width;
  const frag_height = self.compendium.fragment_height;

  const origin_place = Location2i.evoke(
    @floatToInt(i32, @floor(origin.horizon / @intToFloat(f32, frag_width))),
    @floatToInt(i32, @floor(origin.sky / @intToFloat(f32, frag_height)))
  );
  const end_place = Location2i.evoke(
    @floatToInt(i32, @ceil(end.horizon / @intToFloat(f32, frag_width))),
    @floatToInt(i32, @ceil(end.sky / @intToFloat(f32, frag_height)))
  );

  var hor = origin_place.horizon;
  while( hor <= end_place.horizon) : ( hor += 1 ) {
    var sky = origin_place.sky;
    while( sky <= end_place.sky ) : ( sky += 1 ) {
      if( hor < 0 or sky < 0 or hor >= self.width or sky >= self.height )
        continue;

      const symbol = self.transmuteLocationToSymbol(
        @intCast(u32, hor), 
        @intCast(u32, sky)
      );
      const place = self.places[symbol];

      if( !place.exists ) 
        continue;
      
      const texture = self.compendium.recallFragmentTexture(place.fragment);
      var frag_figure = self.compendium.recallFragmentFigure(place.fragment);
      var creation_figure = rl.Rectangle {
        .x = @intToFloat(f32, hor * @intCast(i32, frag_width)),
        .y = @intToFloat(f32, sky * @intCast(i32, frag_height)),
        .width = @intToFloat(f32, frag_width),
        .height = @intToFloat(f32, frag_height)
      };
      var revolution: f32 = 0;

      if( place.invert_width )
        frag_figure.width = -frag_figure.width;
      if( place.invert_height )
        frag_figure.height = -frag_figure.height;
      
      if( place.revolve ) {
        revolution = 90;
        creation_figure.x += @intToFloat(f32, frag_width);
      }

      rl.DrawTexturePro(
        texture, 
        frag_figure, creation_figure, 
        //Location2f.here.transmute(),
        rl.Vector2 { .x = 0, .y = 0 },
        revolution,
        //eg.brain.Light.light.transmute(),
        rl.WHITE,
      );
    }
  }
}



pub const Place = packed struct(u32) {
  fragment: u28 = 0,
  exists: bool = false,
  invert_width: bool = false,
  invert_height: bool = false,
  revolve: bool = false,
};

pub const GrowOptions = struct {
  invert_width: bool = false,
  invert_height: bool = false,
  revolve: bool = false,
};
