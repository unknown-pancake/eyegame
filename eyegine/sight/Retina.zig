
const std = @import("std");
const rl = @import("raylib");
const eg = @import("../eyegine.zig");

const Retina = @This();



width: u32 = 320,
height: u32 = 180,
birth_light: eg.brain.Light = eg.brain.Light.darkness,

texture: rl.RenderTexture2D,



pub fn evoke(
  w: u32,
  h: u32
) Retina {
  const tex = rl.LoadRenderTexture(
    @intCast(c_int, w), 
    @intCast(c_int, h)
  );

  return .{
    .width = w,
    .height = h,
    .texture = tex
  };
}

pub fn banish(
  self: *Retina
) void {
  rl.UnloadRenderTexture(self.texture);
}



pub fn birth(
  self: *Retina
) void {
  rl.ClearBackground(self.birth_light.transmute());
}

pub fn invoke(
  self: *Retina
) void {
  rl.BeginTextureMode(self.texture);
}

pub fn release(
  self: *Retina
) void {
  _ = self;
  rl.EndTextureMode();
}



pub fn imprint(
  self: *Retina
) void {
  const vp_w = @intToFloat(f32, self.width);
  const vp_h = @intToFloat(f32, self.height);
  const sc_w = @intToFloat(f32, rl.GetScreenWidth());
  const sc_h = @intToFloat(f32, rl.GetScreenHeight());

  const ratio_x = vp_w / sc_w;
  const ratio_y = vp_h / sc_h;
  
  const ratio = std.math.max(ratio_x, ratio_y);

  const src_rect = rl.Rectangle {
    .x = 0.0, .y = 0.0,
    .width = vp_w,
    .height = -vp_h,
  };
  const dst_rect = rl.Rectangle {
    .x = sc_w / 2.0 - vp_w / ratio / 2.0,
    .y = sc_h / 2.0 - vp_h / ratio / 2.0,
    .width = vp_w / ratio,
    .height = vp_h / ratio
  };

  rl.DrawTexturePro(
    self.texture.texture, src_rect, dst_rect, 
    rl.Vector2 { .x = 0, .y = 0 },
    0, rl.WHITE
  );
}
