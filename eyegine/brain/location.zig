const std = @import("std");
const rl = @import("raylib");



pub fn Location2(
  comptime T: type
) type {
  return struct {
    const Location2T = @This();


    pub const here = evoke(0, 0);
    pub const step = evoke(1, 1);
    pub const morning = evoke(1, 0);
    pub const evening = evoke(-1, 0);
    pub const noon = evoke(0, -1);
    pub const night = evoke(0, 1);



    horizon: T = 0,
    sky: T = 0,



    pub inline fn evoke(
      h: T, s: T
    ) Location2T {
      return .{ .horizon = h, .sky = s };
    }

    pub inline fn transmute(
      self: Location2T
    ) rl.Vector2 {
      return if( std.meta.trait.isFloat(T) )
        rl.Vector2 { 
          .x = @floatCast(f32, self.horizon), 
          .y = @floatCast(f32, self.sky) 
        }
      else
        rl.Vector2 {
          .x = @intToFloat(f32, self.horizon),
          .y = @intToFloat(f32, self.sky)
        };
    }

    pub inline fn transmuteSoul(
      self: Location2T,
      comptime Target: type
    ) Location2(Target) {
      if( std.meta.trait.isFloat(T) ) {
        return if( std.meta.trait.isFloat(Target) )
          evoke(@floatCast(Target, self.horizon), @floatCast(Target, self.sky))
        else
          evoke(@intToFloat(Target, self.horizon), @floatCast(Target, self.sky));
      } else {
        return if( std.meta.trait.isFloat(Target) )
          evoke(@floatToInt(Target, self.horizon), @floatToInt(Target, self.sky))
        else
          evoke(@intCast(Target, self.horizon), @intCast(Target, self.sky));        
      }
    }
    
  };
}
