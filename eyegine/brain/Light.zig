
const std = @import("std");
const rl = @import("raylib");

const Light = @This();


pub const light = Light{ .fire = 1, .plant = 1, .sky = 1, .amount = 1 };
pub const darkness = evoke(0, 0, 0, 1);



fire: f32 = 0.0,
plant: f32 = 0.0,
sky: f32 = 0.0,
amount: f32 = 0.0,



pub inline fn evoke(
  f: f32, p: f32, s: f32, a: f32
) Light {
  return .{ 
    .fire = f,
    .plant = p,
    .sky = s,
    .amount = a
  };
}



pub inline fn transmute(
  self: Light
) rl.Color {
  const r = @floatToInt(i32, self.fire * 255.0);
  const g = @floatToInt(i32, self.plant * 255.0);
  const b = @floatToInt(i32, self.sky * 255.0);
  const a = @floatToInt(i32, self.amount * 255.0);

  return .{
    .r = @truncate(u8, @bitCast(u32, r)),
    .g = @truncate(u8, @bitCast(u32, g)),
    .b = @truncate(u8, @bitCast(u32, b)),
    .a = @truncate(u8, @bitCast(u32, a)),
  };
}
