
const location = @import("brain/location.zig");

pub const Location2 = location.Location2;
pub const Location2i = location.Location2(i32);
pub const Location2u = location.Location2(u32);
pub const Location2f = location.Location2(f32);

pub const Light = @import("brain/Light.zig");
